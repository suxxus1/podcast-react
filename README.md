# [Mind Sound](https://mind-sound.onrender.com/)

<div align="center">
  <a href="https://mind-sound.onrender.com/">
    <img src="markdown/logo-rgb.svg" width="200" height="200" />
  </a>
</div>

### Front-end technical exercise

The objective of this Exercise is to build an app to listen to podcasts. Content is retrieved from the Apple Itunes api.

[WIKI: description of the exercise](https://gitlab.com/suxxus1/podcast-react/-/wikis/Podcast)

### React:

**_There are 2 versions_**:

#### Stateless components

using

- useReducer hook
- Single state
- Prop drilling, passing data to components
- Sending messages to update state

#### Stateful components

[mind sound 2 repo](https://gitlab.com/suxxus1/mind-sound_2)

using

- useContext (for passing data)
- useState hook
- useMemo hook

**Both versions have pros and cons, it's good to compare both solutions.**

## Clone repo:

```
git clone "https://gitlab.com/suxxus1/podcast-react.git"

```

## Develop:

./frontend/[README](frontend)

./server/[README](server)

## Gitlab-ci:

Documentation is available at ./Gitlab-ci.md

## Technology:

- **Webpack**
- **Storybook** (stand-alone components development)
- **Typescript**
- **Jest**
- **React testing library** (React component testing)
- **MSW** (to mock requests)
- **Cypress** (E2E testing)
- **Docker**
  - to develop the app
  - to run the compiled version locally (frontend & server)
  - to run Gitlab-ci locally

## Demo online:

[Mind Sound](https://mind-sound.onrender.com/)
