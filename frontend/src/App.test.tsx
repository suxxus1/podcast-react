import * as React from "react";
import { render, screen, waitFor } from "@testing-library/react";
import * as helpers from "./utils/helpers";
import * as reducers from "./reducers/reducers";
import {
  FakeStateShoudGetPodcastFromApi,
  FakeStatePodcastWithEpisodes,
  PodcastWithNoEpisodes,
  PodcastDetail,
} from "./mocks/Fixtures";

import { BrowserRouter, MemoryRouter } from "react-router-dom";
import App from "./App";

function renderApp() {
  const promise = Promise.resolve(PodcastWithNoEpisodes);
  jest.spyOn(helpers, "doOnStart").mockReturnValue(promise);
  const utils = render(<App />, { wrapper: BrowserRouter });

  return {
    ...utils,
  };
}

function renderFechingPodcastEpisodes() {
  const promiseEpisodes = Promise.resolve(PodcastDetail.episodes);
  jest.spyOn(helpers, "getEpisodes").mockReturnValue(promiseEpisodes);
  const utils = render(<App />, { wrapper: BrowserRouter });
  return utils;
}

function rendeAppWithRouterHistory(route: string) {
  const utils = render(
    <MemoryRouter initialEntries={[route]}>
      <App />
    </MemoryRouter>
  );
  return { ...utils };
}

describe("<App>", () => {
  describe("Page content", () => {
    test("Header exist", async () => {
      renderApp();

      await waitFor(() => {
        screen.getByTestId("header");
      });
    });
    //
    test("Main view", async () => {
      renderApp();

      const pattern = /Joe Budden Podcast/i;
      expect(screen.queryByText(pattern)).toBeNull();

      await waitFor(() => {
        // data was fetched
        screen.getByText(pattern);
      });
    });
    //
    test("Podcast view", async () => {
      jest
        .spyOn(reducers, "getInitialState")
        .mockReturnValue({ ...FakeStateShoudGetPodcastFromApi });

      renderFechingPodcastEpisodes();

      const episodeTitle = "Episode 611 | Butt Naked or Bounce";
      const episodeDate = /2023-02/;
      const episodeDuration = "0:25:30";

      // list is empty
      expect(screen.queryByText(episodeTitle)).toBeNull();
      expect(screen.queryByText(episodeDate)).toBeNull();
      expect(screen.queryByText(episodeDuration)).toBeNull();

      await waitFor(() => {
        // data was fetched
        screen.getByTestId("podcast-detail-view");
        screen.getByTestId("podcast-detail");
        screen.getByText(/Episodes:/i);
        screen.getByText(/Description:/i);
        screen.getByText(/title/i);
        screen.getByText(/duration/i);
        screen.getByText(/date/i);
      });

      // the list contains expected items
      screen.getByTestId("episodes-list");
      screen.getByText(episodeTitle);
      screen.getByText(episodeDate);
      screen.getByText(episodeDuration);
    });

    test("Episode View", async () => {
      const route = "/podcast/2222/episode/1535809341";
      jest
        .spyOn(reducers, "getInitialState")
        .mockReturnValue({ ...FakeStatePodcastWithEpisodes });

      // do not exist
      const episodeView = "episode-view";
      const episodeTitle = /Episode 611 | Butt Naked or Bounce/i;
      const episodeDescription = /The JBP kicks off this episode recapping/i;
      const podcastDescription = /Description:/;

      expect(screen.queryByTestId(episodeView)).toBeNull();
      expect(screen.queryByText(episodeDescription)).toBeNull();
      expect(screen.queryByText(episodeTitle)).toBeNull();

      // navigate to Episode View
      rendeAppWithRouterHistory(route);

      await waitFor(() => {
        screen.getByTestId(episodeView);
        screen.getByText(episodeTitle);
        screen.getByText(episodeDescription);
        screen.getByText(podcastDescription);
      });
    });

    test("Footer exist", async () => {
      renderApp();
      await waitFor(() => {
        screen.getByText(/mind sound/i);
      });
    });
  });
});
