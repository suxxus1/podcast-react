import React from "react";

export default function Footer(): JSX.Element {
  return (
    <footer className="footer logo-grey">
      <div className="footer__copyright">
        © 2021 Copyright
        <span className="footer__name">Mind Sound</span>
      </div>
    </footer>
  );
}
