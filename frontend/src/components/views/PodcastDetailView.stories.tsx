import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { FakeState, Podcasts } from "../../mocks/Fixtures";
import PodcastDetailView from "./PodcastDetailView";

export default {
  title: "Podcaster/PodcastDetailView",
  component: PodcastDetailView,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof PodcastDetailView>;

const Template: ComponentStory<typeof PodcastDetailView> = (args) => (
  <PodcastDetailView {...args} />
);

export const InitialPodcastDetailView = Template.bind({});
InitialPodcastDetailView.args = {
  ...FakeState,
  podcasts: { ...Podcasts },
  selectedPodcastId: 2222,
};
