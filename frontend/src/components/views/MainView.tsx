import React from "react";
import { IModelUi, Nothing } from "../../types";
import PodcastPreview from "../podcastPreview/PodcastPreview";
import { getPodcastById } from "../../utils/helpers";

export default function MainView({
  podcasts,
  dispatch,
}: IModelUi): JSX.Element {
  return (
    <div className="main-view-container container scroll-bar">
      <ul className="grid-pods-prev">
        {podcasts.ids.map((id) => {
          const pod = getPodcastById(id, podcasts.data);
          const isPod = pod !== Nothing;

          if (isPod) {
            return (
              <li key={id}>
                <PodcastPreview {...pod} dispatch={dispatch} />
              </li>
            );
          } else {
            console.error(`can not find podcast with id: ${id}`);
          }
        })}
      </ul>
    </div>
  );
}
