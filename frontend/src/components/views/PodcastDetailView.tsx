import React from "react";
import { IModelUi, Nothing } from "../../types";
import { getPodcastById } from "../../utils/helpers";
import PodcastDetail from "../podcastDetail/PodcastDetail";
import PodcastEpisodes from "../podcastEpisodes/PodcastEpisodes";

export default function PodcastDetailView(props: IModelUi): JSX.Element {
  const { podcasts, selectedPodcastId, dispatch } = props;

  if (selectedPodcastId !== Nothing) {
    const podcastDetail = getPodcastById(selectedPodcastId, podcasts.data);
    if (podcastDetail !== Nothing) {
      return (
        <div
          data-testid="podcast-detail-view"
          className="podcast-detail-view-container container scroll-bar"
        >
          <PodcastDetail {...podcastDetail} dispatch={(x) => x} />
          <PodcastEpisodes dispatch={dispatch} {...podcastDetail} />
        </div>
      );
    }
  }

  return <></>;
}
