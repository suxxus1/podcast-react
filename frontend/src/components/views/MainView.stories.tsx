import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { FakeState, Podcasts } from "../../mocks/Fixtures";
import MainView from "./MainView";

export default {
  title: "Podcaster/MainView",
  component: MainView,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof MainView>;

const Template: ComponentStory<typeof MainView> = (args) => (
  <MainView {...args} />
);

export const InitialMainView = Template.bind({});
InitialMainView.args = {
  ...FakeState,
  podcasts: Podcasts,
  isSearchFilterVisible: true,
};
