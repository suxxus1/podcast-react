import React from "react";
import { PodUi } from "../../types";

export default function PodcastPreview({
  img,
  id,
  title,
  artist,
  dispatch,
}: PodUi): JSX.Element {
  return (
    <button
      data-testid="podcast-thumbnail"
      onClick={() => {
        dispatch({
          type: "SelectView",
          payload: {
            id,
            view: "Podcast",
          },
        });
      }}
      className="pod-preview  box-shadow"
    >
      <img src={img} alt={title} className="pod-preview__mask" />
      <div className="pod-preview__metadata">
        <h3>{title}</h3>
        <h4>{artist}</h4>
      </div>
    </button>
  );
}
