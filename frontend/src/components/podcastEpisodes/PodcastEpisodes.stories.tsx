import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PodcastDetail } from "../../mocks/Fixtures";
import PodcastEpisodes from "./PodcastEpisodes";

export default {
  title: "Podcaster/PodcastEpisodes",
  component: PodcastEpisodes,
  argTypes: {
    dispatch: { action: "msg" },
  },
} as ComponentMeta<typeof PodcastEpisodes>;

const Template: ComponentStory<typeof PodcastEpisodes> = (args) => (
  <PodcastEpisodes {...args} />
);

export const IntialEpisodesList = Template.bind({});
IntialEpisodesList.args = {
  ...PodcastDetail,
};
